import Vue from "vue";
import VueRouter from "vue-router";
// import Home from "../views/Home.vue";
import Actual from "../views/Actual.vue";
import PerHour from "../views/PerHour.vue";
import PerDay from "../views/PerDay.vue";
import PerMonth from "../views/PerMonth.vue";
import Charts from "../views/Charts.vue";
import Settings from "../views/Settings.vue";
import SystemInfo from "../views/SystemInfo.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "actual",
    component: Actual
  },
  {
    path: "/per-hour",
    name: "per-hour",
    component: PerHour
  },
  {
    path: "/per-day",
    name: "per-day",
    component: PerDay
  },
  {
    path: "/per-month",
    name: "per-month",
    component: PerMonth
  },
  {
    path: "/charts",
    name: "charts",
    component: Charts
  },
  {
    path: "/settings",
    name: "settings",
    component: Settings
  },
  {
    path: "/system-info",
    name: "system-info",
    component: SystemInfo
  }
];

const router = new VueRouter({
  mode: "history",
  routes
});

export default router;
