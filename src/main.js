import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import VueNativeSock from "vue-native-websocket";

const env =
  process.env.NODE_ENV === "development"
    ? "ws://192.168.178.213:81"
    : "ws://" + location.host + ":81/";

Vue.use(VueNativeSock, env, {
  protocol: "arduino",
  store: store,
  reconnection: true
});

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
